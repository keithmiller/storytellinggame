var mentalHealth = 50; 
var bodyWeight = 25;      
var physHealth = 50;      
var socialPoints = 25;      
var schoolPoints = 25;        
var sunburnt = false;
var restart = false;


$(".start").on("click", function(){
	moveOn();
	$("#points").show();
});
$(".restart").on("click", function(){
	$(".ui").removeClass("final");
	$(".ui").addClass("home");
	$(".character").addClass("initial");
	$(".ui").css("left","0")
	newGame();
});
$(".opt").on("click", function(){
	var myClasses = this.classList;
	if(myClasses[3] == "branches"){
		$(".ui").addClass("branch");
		moveOn();
	} else if(myClasses[3] == "branchBack"){
		$(".ui").removeClass("branch");
		moveOn();
	}else {
		moveOn();
	}
	if($(".back").hasClass("volleyball")){
		$(".back").removeClass("volleyball");
		$(".back").addClass("bedroom");
	}
	if($(".shmeldon").hasClass("show")){
		$(".shmeldon").removeClass("show");
	}
	if($(".james").hasClass("show")){
		$(".james").removeClass("show");
	}
});
$(".end").on("click", function(){
	endGame();
});
$(".vball").on("click",function(){
	$(".back").removeClass("bedroom");
	$(".back").addClass("volleyball");
});
$(".shmeldonShow").on("click",function(){
	$(".shmeldon").addClass("show");
});
$(".jamesShow").on("click",function(){
	$(".james").addClass("show");
});
$(".bothShow").on("click",function(){
	$(".shmeldon").addClass("show");
	$(".james").addClass("show");
});
// $(".vballOff").on("click",function(){
// 	$(".back").removeClass("volleyball");
// 	$(".back").addClass("bedroom");
// 	console.log("vball off");
// });

function newGame(){
	mentalHealth = 50; 
	bodyWeight = 25;      
	physHealth = 50;      
	socialPoints = 25;      
	schoolPoints = 25;        
	sunburnt = false;
	restart = true;
	updateImage();
}

function moveOn(){
	$( ".ui" ).css( "left", "-=900" );
	
	console.log("continued");
}

function updateHealth(mh,bw,ph,ScP,SoP,burnt){
	mentalHealth += mh; 
	if (bodyWeight<15 && ph>0) {
		bodyWeight += 10
	} else if(bodyWeight<30 && bw<0 && ph>0){
		 bodyWeight += (.5 * bw); 
	} else {
		bodyWeight += bw; 
	}
	    
	physHealth += ph;      
	socialPoints += SoP;      
	schoolPoints += ScP;


	if (burnt) {
		sunburnt = true;
	}else{
		sunburnt = false; 
	}

	updateImage();
}

function updateImage(){ 
	



	console.log("mh = "+ mentalHealth+", bw = "+bodyWeight+", ph = "+physHealth+", ScP = "+schoolPoints+". SoP = "+socialPoints);
		
	if (!restart && sunburnt) {
		if ($(".character").hasClass("plain")) {
			$(".character").removeClass("plain");
			$(".character").addClass("sunburn");
		} else if ($(".character").hasClass("sick")) {
			$(".character").removeClass("sick");
			$(".character").addClass("sunburn");
		}
	} else {
		if ($(".character").hasClass("sunburn")) {
			$(".character").removeClass("sunburn");
			$(".character").addClass("plain");
			restart = false;
		} else if ($(".character").hasClass("sick")) {
			$(".character").removeClass("sick");
			$(".character").addClass("plain");
			restart = false;
		}
	}
	if(physHealth<25){
		if ($(".character").hasClass("plain")) {
			$(".character").removeClass("plain");
			$(".character").addClass("sick");
		} else if ($(".character").hasClass("sunburn")) {
			$(".character").removeClass("sunburn");
			$(".character").addClass("sick");
		}
	} else {
		if ($(".character").hasClass("sick")) {
			$(".character").removeClass("sick");
			$(".character").addClass("plain");
		} 
	}
	if(mentalHealth<30 || socialPoints<0 || schoolPoints<10 || ( (socialPoints + schoolPoints + physHealth) < 70)){
		if ($(".character").hasClass("happy")) {
			$(".character").removeClass("happy");
			$(".character").addClass("sad");
		} 
	} else {
		if ($(".character").hasClass("sad")) {
			$(".character").removeClass("sad");
			$(".character").addClass("happy");
		} 
	}


	if(socialPoints>25 && socialPoints<35){
		$(".social.decor.lvl1").addClass("show");
		$(".social.decor.lvl2").removeClass("show");
		$(".social.decor.lvl3").removeClass("show");
	} else if(socialPoints>=35 && socialPoints<=55){
		$(".social.decor.lvl1").addClass("show");
		$(".social.decor.lvl2").addClass("show");
		$(".social.decor.lvl3").removeClass("show");
	} else if(socialPoints>55){
		$(".social.decor.lvl3").addClass("show");
	} else {
		$(".social.decor.lvl1").removeClass("show");
		$(".social.decor.lvl2").removeClass("show");
		$(".social.decor.lvl3").removeClass("show");
	}
	
	if(schoolPoints<5){
		$(".school.decor.lvl1").addClass("show");
		$(".school.decor.lvl2").removeClass("show");
		
	} else if(schoolPoints>=5 && schoolPoints<20){
		$(".school.decor.lvl2").addClass("show");
		$(".school.decor.lvl3").removeClass("show");
		$(".school.decor.lvl1").removeClass("show");
	} else if(schoolPoints>=20 && schoolPoints<35){
		$(".school.decor.lvl3").addClass("show");
		$(".school.decor.lvl2").removeClass("show");
		$(".school.decor.lvl4").removeClass("show");
	} else if(schoolPoints>=35 && schoolPoints<=50){
		$(".school.decor.lvl4").addClass("show");
		$(".school.decor.lvl3").removeClass("show");
		$(".school.decor.lvl5").removeClass("show");
	} else if(schoolPoints>50){
		$(".school.decor.lvl5").addClass("show");
		$(".school.decor.lvl4").removeClass("show");
	} else {
		alert("schoolpoints: " + schoolPoints);
	}

	if(bodyWeight>15 && bodyWeight<30){
		if ($(".character").hasClass("fat")) {
			$(".character").removeClass("fat");
			$(".character").addClass("regular");
		} else if ($(".character").hasClass("skinny")) {
			$(".character").removeClass("skinny");
			$(".character").addClass("regular");
		} else if ($(".character").hasClass("kindaFat")) {
			$(".character").removeClass("kindaFat");
			$(".character").addClass("regular");
		}
	} else if(bodyWeight>=30 && bodyWeight<=35){
		if ($(".character").hasClass("regular")) {
			$(".character").removeClass("regular");
			$(".character").addClass("kindaFat");
		} else if ($(".character").hasClass("fat")) {
			$(".character").removeClass("fat");
			$(".character").addClass("kindaFat");
		}
	} else if(bodyWeight>35){
		if ($(".character").hasClass("kindaFat")) {
			$(".character").removeClass("kindaFat");
			$(".character").addClass("fat");
		} 
	} else if(bodyWeight<=15){
		if ($(".character").hasClass("regular")) {
			$(".character").removeClass("regular");
			$(".character").addClass("skinny");
		} 
	}



	// $(".character").css("background","")

}
function endGame(){

	if ($(".character").hasClass("plain")) {
		$("#skinSentence").html("Thankfully you're physical health is in a good place.");
	} else if($(".character").hasClass("sick")){
		if(physHealth<10){
			$("#skinSentence").html("In doing everything else you've totally neglected your immune system and are constantly getting sick.");
		} else{
			$("#skinSentence").html("Sadly you haven't paid enough attention to your physical health and often get sick.");
		}
		
	} else {
		$("#skinSentence").html("You are sunburnt. Skin health is often overlooked and is important in the long run.");
	}
	
	if ($(".character").hasClass("happy")) {
		$("#mhSentence").html("Overall, you haven't let life get you down.");
	} else {
		$("#mhSentence").html("Overall, life hasn't been great and you are sad.");
	}


	if(socialPoints>25 && socialPoints<35){
		$("#socSentence").html("Your social life has been decent.");
	} else if(socialPoints>=35 && socialPoints<=55){
		$("#socSentence").html("Your social life has been pretty wild.");
	} else if(socialPoints>55){
		$("#socSentence").html("Your social life has been off the chain.");
	} else {
		$("#socSentence").html("Sadly, you no longer have many friends, as your social life has been suffering.");
	}
	
	if(schoolPoints<5){
		$("#schoolSentence").html("You've totally neglected your school work and will soon fail out of school, wasting your parent's thousands of dollars.");
		
	} else if(schoolPoints>=5 && schoolPoints<20){
		$("#schoolSentence").html("Your academic situation has been badly suffering and if you don't start improving you might soon fail out.");
	} else if(schoolPoints>=20 && schoolPoints<35){
		$("#schoolSentence").html("Your school performance has been passable, but nothing to write home about.");
	} else if(schoolPoints>=35 && schoolPoints<=50){
		$("#schoolSentence").html("You've done pretty well in school and can feel satisfied in your academic situation.");
	} else if(schoolPoints>50){
		$("#schoolSentence").html("You're school work has been impeccable, and you're admired by the other nerds you know.");
	} else {
		alert("schoolpoints: " + schoolPoints);
	}


	if ($(".character").hasClass("fat")) {
		$("#bwSentence").html("Freshman 15? more like Freshman 50. Maybe skip the pizza some days?");
	} else if($(".character").hasClass("skinny")) {
		$("#bwSentence").html("You were so afraid of gaining weight that you barely ate anything and now look like a skeleton.");
	} else if($(".character").hasClass("regular")) {
		$("#bwSentence").html("You've managed to keep a fairly balanced diet and active lifestyle. Good job!");
	} else {
		$("#bwSentence").html("Unfortunately, you ended up gaining the dreaded freshman 15, but it's nothing some good diet and exercise couldn't reverse.");
	}
}
