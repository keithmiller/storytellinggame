# README #

### What is this repository for? ###

* This is a text based game we designed and built in about a month. The idea is that you are in the shoes of a college student and must make daily decisions, but after each one you see how it affects your health, happiness, academics, and social life. This is the first time I've made a text based game in HTML from scratch, so I think next time, with more time I would structure it to make it more reusable and easily edited.
* 1.0


### How do I get set up? ###

Just open index.html in a browser
